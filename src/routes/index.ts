/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express'
import helloRouter from './HelloRouter'
import { LogInfo } from '../utils/logger'
import goodbyeRouter from './GoodbyeRouter'
import usersRouter from './UserRouter'
import katasRouter from './KataRouter'
import authRouter from './AuthRouter'

// Server instance
const server = express()

// Router instance
const rootRouter = express.Router()

// Activate for requests to http://localhost:8000/api

// GET: http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http://localhost:8000/api')
  // Send Hello World
  res.send('Welcome to API Restful Express + Nodemon + Jest + TS + Swagger + Mongoose')
})

// REdirections to Router & Controllers
server.use('/', rootRouter) // http://localhost:8000/api
server.use('/hello', helloRouter) // http://localhost:8000/api/hello
server.use('/goodbye', goodbyeRouter) // http://localhost:8000/api/goodbye
server.use('/users', usersRouter) // http://localhost:8000/api/users
server.use('/katas', katasRouter) // http://localhost:8000/api/katas
server.use('/auth', authRouter) // http://localhost:8000/api/auth
// Add more routes

export default server
