import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'
import { LogInfo } from '../utils/logger'

// Middleware
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Router from express
const usersRouter = express.Router()

// http://localhost:8000/api/users/?id=62541fb5c384597418cff8db
usersRouter.route('/')
// GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param
    const id: any = req?.query?.id
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    LogInfo(`Query Param: ${id}, ${page} ${limit}`)
    // Controller Instance to execute method
    const controller: UserController = new UserController()
    // Obtain Response
    const response: any = await controller.getUser(page, limit, id)
    // Send to the client the response
    return res.status(200).send(response)
  })
  // DELETE
  .delete(verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const id: any = req?.query.id
    LogInfo(`Query Param: ${id}`)
    // Controller Instance to execute method
    const controller: UserController = new UserController()
    // Obtain Response
    const response: any = await controller.deleteUser(id)
    // Send to the client the response
    return res.status(200).send(response)
  })
  // PUT
  .put(verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const id: any = req?.query?.id
    const name: any = req?.query.name
    const age: any = req?.query.age
    const email: any = req?.query.email
    LogInfo(`Query Params: ${id}, ${name}, ${age}, ${email}`)
    // Controller Instance to execute method
    const controller: UserController = new UserController()
    const user = {
      name: name,
      email: email,
      age: age
    }
    // Obtain Response
    const response: any = await controller.updateUser(id, user)
    // Send to the client the response
    return res.status(200).send(response)
  })

// http://localhost:8000/api/users/katas
usersRouter.route('/katas')
// GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param
    const id: any = req?.query?.id
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    LogInfo(`Query Param: ${id}, ${page} ${limit}`)
    // Controller Instance to execute method
    const controller: UserController = new UserController()
    // Obtain Response
    const response: any = await controller.getKata(page, limit, id)
    // Send to the client the response
    return res.status(200).send(response)
  })
// Export Hello Router
export default usersRouter

/**
 * Get documents => 200 OK
 * Creation documents => 201 OK
 * Delete Documents => 200 (Entity) / 204 No return
 * Update Documents => 200 (Entity) / 204 No return
 */
