import express, { query, Request, Response } from 'express'
import { KataController } from '../controller/KatasController'
import { LogInfo } from '../utils/logger'
// Middleware
import { verifyToken } from '../middlewares/verifyToken.middleware'
// Body parser
import bodyParser from 'body-parser'
import { IKata, KataLevel } from '../domain/interfaces/IKata.interface'

// File Uploader
import fileUpload from 'express-fileupload'

const jsonParser = bodyParser.json()
// Router from express
const katasRouter = express.Router()

// http://localhost:8000/api/katas/
katasRouter.route('/')
// GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param
    const id: any = req?.query.id
    const level: any = req?.query.level
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    LogInfo(`Query Param: ${id}`)
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getKata(page, limit, id, level)
    // Send to the client the response
    return res.send(response)
  })
  // DELETE
  .delete(verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const id: any = req?.query.id
    const userID: any = req.query.userID
    const userRol: any = req.query.userRol
    LogInfo(`Query Param: ${id}`)
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.deleteKata(id, userID, userRol)
    // Send to the client the response
    return res.send(response)
  })
  // POST
  .post(jsonParser, verifyToken, async (req:Request, res:Response) => {
    const name: string = req?.body?.name
    const description: string = req?.body?.description || 'Default Description'
    const level: KataLevel = req?.body?.level || KataLevel.BASIC
    const creator: any = req.query?.userID
    const solution: string = req?.body?.solution || 'Default Solution'
    if (name && description && level && creator && solution) {
      // Controller Instance to execute method
      const controller: KataController = new KataController()
      const kata:IKata = {
        name: name,
        description: description,
        level: level,
        tries: 0,
        stars: { average: 0, users: [] },
        creator: creator,
        solution: solution,
        participants: []
      }
      // Obtain Response
      const response: any = await controller.createKata(kata)
      // Send to the client the response
      return res.status(201).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Creating Kata. You need to send all the attrs of Kata to create'
      })
    }
  })
  // PUT
  .put(jsonParser, verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const id: any = req?.query.id
    const name: string = req?.body?.name
    const description: string = req?.body?.description || ''
    const level: KataLevel = req?.body?.level || KataLevel.BASIC
    const solution: string = req?.body?.solution || ''
    const userID: any = req.query.userID
    const userRol: any = req.query.userRol

    if (name && description && level && solution) {
      // Controller Instance to execute method
      const controller: KataController = new KataController()
      const kata:IKata = {
        name: name,
        description: description,
        level: level,
        tries: 0,
        stars: { average: 0, users: [{ user: '', stars: 0 }] },
        creator: '',
        solution: solution,
        participants: []
      }
      // Obtain Response
      const response: any = await controller.updateKata(id, kata, userID, userRol)
      // Send to the client the response
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Updating Kata. You need to send all the attrs of Kata to update'
      })
    }
  })

// http://localhost:8000/api/katas/level
katasRouter.route('/level')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const direction: any = req?.query.direction
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getSortLevelKatas(direction, page, limit)
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/stars
katasRouter.route('/stars')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const direction: any = req?.query.direction
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getSortStarsKatas(direction, page, limit)
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/vote
katasRouter.route('/vote')
// PUT
  .put(verifyToken, async (req:Request, res:Response) => {
  // Obtain a Query Param
    const id: any = req?.query.id
    const vote: any = req?.query.vote
    const userID: any = req.query?.userID
    LogInfo(`Query Params: ${id}, ${vote}`)
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.voteKata(id, vote, userID)
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/solve
katasRouter.route('/solve')
// PUT
  .put(jsonParser, verifyToken, async (req:Request, res:Response) => {
  // Obtain a Query Param
    const id: any = req?.query.id
    const solution: string = req?.body?.solution
    const userID: any = req.query?.userID
    LogInfo(`Query Params: ${id}`)
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.solveKata(id, solution, userID)
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/upload
katasRouter.route('/upload')
// Post
  .post(jsonParser, async (req:Request, res:Response) => {
    const files: any = req?.body?.files
    console.log(req)
    try {
      if (!files) {
        res.send({
          status: false,
          message: 'There was no file found in request',
          payload: {}
        })
      } else {
        // Use the name of the input field (i.e. "file") to retrieve the uploaded file
        const file = files.file
        // Use the mv() method to place the file in upload directory (i.e. "uploads")
        file.mv('./uploads/' + file.name)
        // send response
        res.send({
          status: true,
          message: 'File was uploaded successfuly',
          payload: {
            name: file.name,
            mimetype: file.mimetype,
            size: file.size
            // path: '/files/uploads/'
            // url: 'https://my-ftp-server.com/bjYJGFYgjfVGHVb'
          }
        })
      }
    } catch (err) {
      res.status(500).send({
        status: false,
        message: 'Unexpected problem',
        payload: {}
      })
    }
  })

// NOT USED
// NOT USED
// NOT USED
// NOT USED

// http://localhost:8000/api/katas/lasts
katasRouter.route('/lasts')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getKataLast()
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/bests
katasRouter.route('/bests')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getKataBest()
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/tries
katasRouter.route('/tries')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getKataTries()
    // Send to the client the response
    return res.send(response)
  })

// http://localhost:8000/api/katas/tries
katasRouter.route('/tries')
// Get
  .get(verifyToken, async (req:Request, res:Response) => {
    // Controller Instance to execute method
    const controller: KataController = new KataController()
    // Obtain Response
    const response: any = await controller.getKataTries()
    // Send to the client the response
    return res.send(response)
  })
// Export Hello Router
export default katasRouter
