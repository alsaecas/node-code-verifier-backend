import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'
import { LogInfo } from '../utils/logger'
import { IUser, RolType } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'

// BCRYPT for passwords
import bcrypt from 'bcrypt'
import { AuthController } from '../controller/AuthController'

// Middleware
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Body parser
import bodyParser from 'body-parser'
const jsonParser = bodyParser.json()

// Router from express
const authRouter = express.Router()

// http://localhost:8000/api/auth/register
authRouter.route('/register')
// POST
  .post(jsonParser, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const { name, email, password, age } = req?.body
    let hashedPassword = ''
    if (name && email && password && age) {
      // Obtain the password from the request and cypher
      hashedPassword = bcrypt.hashSync(password, 8)
      const newUser: IUser = {
        id: '',
        name: name,
        email: email,
        password: hashedPassword,
        age: age,
        katas: [],
        rol: RolType.USER
      }
      // Controller Instance to execute method
      const controller: AuthController = new AuthController()
      // Obtain Response
      const response: any = await controller.registerUser(newUser)
      // Send to the client the response
      if (response) {
        return res.status(200).send(response)
      } else {
        return res.status(400).send({
          message: '[ERROR]The User is Already Registered'
        })
      }
    } else {
      return res.status(400).send({
        message: '[ERROR]No User can be registered'
      })
    }
  })

// http://localhost:8000/api/auth/login
authRouter.route('/login')
// POST
  .post(jsonParser, async (req:Request, res:Response) => {
    // Obtain a Query Param
    const { email, password } = req?.body
    if (email && password) {
      // Controller Instance to execute method
      const controller: AuthController = new AuthController()
      const auth: IAuth = {
        email: email,
        password: password
      }
      // Obtain Response
      const response: any = await controller.loginUser(auth)
      // Send to the client the response
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR]No User can be loged in'
      })
    }
  })

// Protected Route
authRouter.route('/me')
  .get(verifyToken, async (req:Request, res:Response) => {
    // Obtain ID of User to check data
    const id: any = req?.query?.id
    if (id) {
      const controller: AuthController = new AuthController()
      // Obtain response
      const response: any = await controller.userData(id)
      // Send to the client the response
      return res.status(200).send(response)
    } else {
      return res.status(401).send({
        message: '[ERROR]Not Authorised'
      })
    }
  })

export default authRouter
