import { Get, Post, Query, Route, Tags } from 'tsoa'
import { IAuthController } from './interfaces'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'

// ORM - Users Collection
import { getUserByID, getUserByNameOrEmail, registerUser } from '../domain/orm/User.orm'
import { loginUser, logoutUser } from '../domain/orm/Auth.orm'
import { LogSuccess, LogWarning, LogError } from '../utils/logger'
import { AuthResponse, ErrorResponse } from './types'

@Route('/api/auth')
@Tags('AuthController')
export class AuthController implements IAuthController {
  /**
   * Endpoint to retrieve Users in the Collection "Users" of DB
   * @param {IUser} user User to register.
   * @returns All users or user found by ID
   */
   @Post('/register')
  public async registerUser (@Query()user: IUser): Promise<any> {
    let response: any = ''
    let userFound: boolean = false
    if (user) {
      await getUserByNameOrEmail(user.name, user.email).then((r) => {
        if (r) {
          userFound = true
          LogWarning('[/api/auth/register] User Already registered')
          return {
            message: `User Already registered: ${user.name}`
          }
        }
      })
      if (!userFound) {
        LogSuccess(`[/api/auth/register] register user:  ${user.email}`)
        await registerUser(user).then((r) => {
          LogSuccess(`[/api/auth/register] Create User: ${user.email}`)
          response = {
            message: `User created succesfully: ${user.name}`
          }
        })
      }
    } else {
      LogWarning('[/api/auth/register] Register user without USER')
      return {
        message: 'Pelase, provide the user to register. '
      }
    }
    return response
  }

   /**
   * Endpoint to retrieve Users in the Collection "Users" of DB
   * @param {IAuth} auth User to register.
   * @returns All users or user found by ID
   */
   @Post('/login')
   public async loginUser (@Query()auth: IAuth): Promise<any> {
     let response: AuthResponse | ErrorResponse | undefined
     if (auth) {
       LogSuccess(`[/api/auth/login] Login user: ${auth.email}`)
       const data = await loginUser(auth)
       try {
         response = {
           token: data.token,
           message: `Welcome, ${data.user.name}`,
           userID: data.user._id
         }
       } catch (error) {
         LogError('[ERROR in Authoritation]: User not Found')
       }
     } else {
       LogWarning('[/api/auth/login] Login user without Auth')
       return {
         error: 'Not Valid',
         message: 'Pelase, provide the auth (email & pass) to login. '
       }
     }
     return response
   }

   /**
   * Endpoint to retrieve the User in the Collection "Users" of DB
   * Middleware with token validation (x-access token with valid token)
   * @param {string} id Id of user to retrieve
   * @returns All users or user found by ID
   */
  @Get('/me')
   public async userData (@Query()id: string): Promise<any> {
     let response: any = ''
     if (id) {
       LogSuccess(`[/api/users] Get User by ID: ${id}`)
       response = await getUserByID(id)
     }
     return response
   }

  /**
   * Endpoint to retrieve Users in the Collection "Users" of DB
   * @param {IUser} user User to register.
   * @returns All users or user found by ID
   */
    @Post('/logout')
  public async logoutUser (@Query()auth: any): Promise<any> {
    return {
      message: 'Good Bye!'
    }
  }
}
