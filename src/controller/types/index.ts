/**
 * Basic JSON response for Controllers
 */
export type BasicResponse = {
  message: string
}

/**
 * Data included response for Controllers
 */
export type GoodbyeResponse = {
  message: string,
  Date: string
}

/**
 * Error response for Controllers
 */
export type ErrorResponse = {
    error: string,
    message: string
  }

/**
 * Auth JSON Response
 */
export type AuthResponse = {
  message: string,
  token: string,
  userID: string
}
