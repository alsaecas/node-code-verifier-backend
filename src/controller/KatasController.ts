import { Delete, Get, Post, Query, Route, Tags, Put } from 'tsoa'
import { IKataController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'

// ORM - Katas Collection
import { getAllKatas, getKataByID, deleteKataByID, createKata, updateKataByID, getKataByLevel, getKataByStars, getKataSortLevel, getKataSortStars, getLastsKatas, getBestsKatas, getTriesKatas, voteKataByID, solveKataByID } from '../domain/orm/Kata.orm'
import { IKata } from '../domain/interfaces/IKata.interface'

@Route('/api/katas')
@Tags('KataController')
export class KataController implements IKataController {
  /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @param {string} id Id of kata to retrieve (optional)
   * @param {number} level Level of kata to retrieve (optional)
   * @returns All katas or kata found by ID
   */
  @Get('/')
  public async getKata (@Query()page: number, @Query()limit: number, @Query()id?: string, @Query()level?: number, @Query()stars?: number): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/katas] Get Kata by ID: ${id}`)
      response = await getKataByID(id)
    } else if (level) {
      LogSuccess(`[/api/katas] Get Kata by Level: ${level}`)
      response = await getKataByLevel(level, page, limit)
    } else if (level) {
      LogSuccess(`[/api/katas] Get Kata by Stars: ${stars}`)
      response = await getKataByStars(level, page, limit)
    } else {
      LogSuccess('[/api/katas] Get All Katas Requests')
      response = await getAllKatas(page, limit)
    }

    return response
  }

  /**
   * Endpoint to delete an Kata in the Collection "Katas" of DB
   * @param {string} id Id of kata to delete (optional)
   * @returns message informing if deletion was successfull
   */
  @Delete('/')
  public async deleteKata (@Query()id?: string, userID?: any, userRol?: any): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/katas] Delete Kata by ID: ${id}`)
      await deleteKataByID(id, userID, userRol).then((r) => {
        response = {
          message: `Kata deleted succesfully with ID: ${id}`
        }
      })
    } else {
      LogWarning('[/api/katas] Delete Kata Request WITHOUT ID')
      return {
        message: 'Pelase, provide an ID to remove from the Database '
      }
    }
    return response
  }

  /**
   * Endpoint to create an Kata in the Collection "Katas" of DB
   * @param {kata} kata Data from kata to create (optional)
   * @returns message informing if creatinon was successfull
   */
  @Post('/')
  public async createKata (@Query()kata: IKata): Promise<any> {
    let response: any = ''
    await createKata(kata).then((r) => {
      LogSuccess(`[/api/katas] Create Kata: ${kata}`)
      response = {
        message: `Kata created succesfully: ${kata.name}`
      }
    })
    return response
  }

  /**
   * Endpoint to update a Kata in the Collection "Katas" of DB
   * @param {string} id Id of kata to update
   * @param {kata} kata Date from kata to update update
   * @returns message informing if update was successfull
   */
  @Put('/')
  public async updateKata (@Query()id: string, @Query()kata: IKata, userID?: any, userRol?: any): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/katas] Update Kata by ID: ${id}`)
      await updateKataByID(id, kata, userID, userRol).then((r) => {
        response = {
          message: `Kata updated succesfully with ID: ${id}`
        }
      })
    } else {
      LogWarning('[/api/katas] Update Kata Request WITHOUT ID')
      return {
        message: 'Pelase, provide an ID to update on the Database '
      }
    }
    return response
  }

  /**
   * Endpoint to upload a Kata file
   * @returns message informing if update was successfull
   */
    @Post('/upload')
  public async updateKataFile (): Promise<any> {

  }

    /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @returns Katas sortened by level
   */
   @Get('/level')
    public async getSortLevelKatas (direction: number, page: number, limit: number): Promise<any> {
      if (direction == -1) {
        LogSuccess('[/api/katas/level] Get Katas sortened by level. From HIGH to BASIC')
      } else {
        LogSuccess('[/api/katas/level] Get Katas sortened by level. From BASIC to HIGH')
      }

      return await getKataSortLevel(direction, page, limit)
    }

   /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @returns Katas sortened by stars
   */
    @Get('/stars')
   public async getSortStarsKatas (direction: number, page: number, limit: number): Promise<any> {
     if (direction == -1) {
       LogSuccess('[/api/katas/stars] Get Katas sortened by stars. From 5 to 0')
     } else {
       LogSuccess('[/api/katas/stars] Get Katas sortened by stars. From 0 to 5')
     }

     return await getKataSortStars(direction, page, limit)
   }

    /**
   * Endpoint to vote a Kata in the Collection "Katas" of DB
   * @param {string} id Id of kata to update
   * @param {vote} vote Vote to give to the Kata
   * @returns message informing if voting was successfull
   */
       @Put('/vote')
    public async voteKata (@Query()id: string, @Query()vote: any, @Query()userID: any): Promise<any> {
      let response: any = ''
      if (id && vote) {
        LogSuccess(`[/api/katas] Voting Kata by ID: ${id}`)
        await voteKataByID(id, vote, userID).then((r) => {
          if (r.kata) {
            response = {
              message: `Kata with ID: ${id} was voted succesfully by user: ${userID}`,
              average: r.kata.stars.average
            }
          } else {
            response = {
              message: r.message
            }
          }
        })
      } else {
        LogWarning('[/api/katas] Vote Kata Request WITHOUT ID')
        return {
          message: 'Pelase, provide an ID and a vote to vote the Kata '
        }
      }
      return response
    }

       /**
   * Endpoint to vote a Kata in the Collection "Katas" of DB
   * @param {string} id Id of kata to update
   * @param {vote} solution Vote to give to the Kata
   * @returns message informing if voting was successfull
   */
         @Put('/solve')
       public async solveKata (@Query()id: string, @Query()solution: any, @Query()userID: any): Promise<any> {
         let response: any = ''
         if (id && solution) {
           LogSuccess(`[/api/katas] Solving Kata by ID: ${id}`)
           await solveKataByID(id, solution, userID).then((r) => {
             response = {
               message: 'r.message'
             }
           })
         } else {
           LogWarning('[/api/katas] Solve Kata Request WITHOUT ID')
           return {
             message: 'Pelase, provide an ID and a solution to solve the Kata '
           }
         }
         return response
       }
         // NOT USED
         // NOT USED
         // NOT USED
         // NOT USED
         // NOT USED

   /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @returns Lasts 5 added Katas
   */
   @Get('/latsts')
         public async getKataLast (): Promise<any> {
           LogSuccess('[/api/katas/lasts] Get lasts 5 Katas')
           return await getLastsKatas()
         }

   /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @returns Lasts 5 added Katas
   */
   @Get('/bests')
   public async getKataBest (): Promise<any> {
     LogSuccess('[/api/katas/lasts] Get Katas sortened by valoration')
     return await getBestsKatas()
   }

   /**
   * Endpoint to retrieve Katas in the Collection "Katas" of DB
   * @returns Lasts 5 added Katas
   */
  @Get('/tries')
   public async getKataTries (): Promise<any> {
     LogSuccess('[/api/katas/lasts] Get Katas sortened by chances')
     return await getTriesKatas()
   }
}
