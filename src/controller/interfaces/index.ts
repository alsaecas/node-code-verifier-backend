import { BasicResponse, GoodbyeResponse } from '../types'
import { IUser } from '../../domain/interfaces/IUser.interface'
import { IKata } from '../../domain/interfaces/IKata.interface'

export interface IHelloController {
  getMessage(name?:string): Promise<BasicResponse>
}

export interface IGoodbyeController {
  getMessage(name?:string): Promise<GoodbyeResponse>
}

export interface IUserController {
  // Read all users from database or from ID
  getUser(page: number, limit:number, id: string): Promise<any>
  // Get all katas from a user
  getKata(page: number, limit:number, id: string): Promise<any>
  // Delete User by ID
  deleteUser(id?:string): Promise<any>
  // Update User
  updateUser(id:string, user:any): Promise<any>
}

export interface IKataController {
  // Read all Katas from database or from ID
  getKata(page: number, limit:number, id?: string): Promise<any>
  // Delete Kata by ID
  deleteKata(id?:string): Promise<any>
  // Create Kata
  createKata(kata:IKata): Promise<any>
  // Update Kata
  updateKata(id:string, kata:IKata, userID: any): Promise<any>
  // Upload file to Kata
  updateKataFile(): Promise<any>
}

export interface IAuthController {
  // Register user
  registerUser(user: IUser): Promise<any>
  // Login user
  loginUser(auth: any): Promise<any>
}
