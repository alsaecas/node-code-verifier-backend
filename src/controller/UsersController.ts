import { Delete, Get, Query, Route, Tags, Put } from 'tsoa'
import { IUserController } from './interfaces'
import { LogError, LogSuccess, LogWarning } from '../utils/logger'

// ORM - Users Collection
import { getAllUsers, getUserByID, deleteUserByID, updateUserByID, getKatasFromUser } from '../domain/orm/User.orm'

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Endpoint to retrieve Users in the Collection "Users" of DB
   * @param {string} id Id of user to retrieve (optional)
   * @returns All users or user found by ID
   */
  @Get('/')
  public async getUser (@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Get User by ID: ${id}`)
      response = await getUserByID(id)
    } else {
      LogSuccess('[/api/users] Get All Users Requests')
      response = await getAllUsers(page, limit)
    }
    return response
  }

  /**
   * Endpoint to delete an User in the Collection "Users" of DB
   * @param {string} id Id of user to delete (optional)
   * @returns message informing if deletion was successfull
   */
  @Delete('/')
  public async deleteUser (@Query()id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Delete User by ID: ${id}`)
      await deleteUserByID(id).then((r) => {
        response = {
          message: `User deleted succesfully with ID: ${id}`
        }
      })
    } else {
      LogWarning('[/api/users] Delete User Request WITHOUT ID')
      return {
        message: 'Pelase, provide an ID to remove from the Database '
      }
    }
    return response
  }

  /**
   * Endpoint to create an User in the Collection "Users" of DB
   * @param {string} id Id of user to update
   * @param {user} user Date from user to update update
   * @returns message informing if update was successfull
   */
  @Put('/')
  public async updateUser (@Query()id: string, @Query()user: any): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Update User by ID: ${id}`)
      await updateUserByID(id, user).then((r) => {
        response = {
          message: `User updated succesfully with ID: ${id}`
        }
      })
    } else {
      LogWarning('[/api/users] Update User Request WITHOUT ID')
      return {
        message: 'Pelase, provide an ID to update on the Database '
      }
    }
    return response
  }

  /**
   * Endpoint to retrieve Katas from one user
   * @param {string} id Id of user to retrieve (optional)
   * @returns All users or user found by ID
   */
    @Get('/katas')
  public async getKata (@Query()page: number, @Query()limit: number, @Query()id: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/usersKatas] Get Katas from user: ${id}`)
      response = await getKatasFromUser(page, limit, id)
    } else {
      LogError('[/api/users/katas] Get Katas from User WITHOUT ID')
      // response = await getAllUsers(page, limit)
      response = {
        message: 'ID from user Needed'
      }
    }
    return response
  }
}
