import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'
// Enviroment variables
import dotenv from 'dotenv'
dotenv.config()
const secret = process.env.SECRETKEY || 'MYSECCRETEKEY'
/**
 * @param { Request } req Original request previous middleware of verification JWT
 * @param { Response } res Response to verification JWT
 * @param { NextFunction } next Next function to be executed
 * @returns Errors of verification or next function
 */
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  // Check Header from the Request for 'x-access-token'
  const token: any = req.headers['x-access-token']

  // Verify if token is present
  if (!token) {
    return res.status(403).send({
      authentication: 'Missing Token',
      message: 'Not authorised to consime this endpoint'
    })
  }

  // Verify the token obtained
  jwt.verify(token, secret, (err: any, decoded: any) => {
    if (err) {
      return res.status(500).send({
        authentication: 'JWT Verification Failed',
        message: 'Failed to verify JWT Token'
      })
    }
    // Pass something to next request (id of user || other info)
    // Execute Next function -> Protected Routes will be executed
    req.query.userID = decoded.id
    req.query.userRol = decoded.rol
    next()
  })
}
