import express, { Express, Request, Response } from 'express'

// Swagger
import swaggerUI from 'swagger-ui-express'

// Security
import cors from 'cors'
import helmet from 'helmet'

// TODO HTTPS

// Root Router
import rootRouter from '../routes'
import mongoose from 'mongoose'

// Create express APP
const server: Express = express()

// Swagger Config route
server.use(
  '/docs',
  swaggerUI.serve,
  swaggerUI.setup(undefined, {
    swaggerOptions: {
      url: '/swagger.json',
      explorer: true
    }
  })
)

// Define SERVER to use /api and use rootRouter from  index.ts in routers
// From this point onover: http://localhost:8000/api/...
server.use(
  '/api',
  rootRouter
)

// Static Server
server.use(express.static('public'))

// TODO Mongoose Connection
mongoose.connect('mongodb://localhost:27017/codeverification')

// Security Config
server.use(helmet())
server.use(cors())

// Conten type:
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// Redirections
// http://localhost:8000 ---> http://localhost:8000/api/
server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server
