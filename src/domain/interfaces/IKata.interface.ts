export enum KataLevel {
    BASIC = 'Basic',
    MEDIUM = 'Medium',
    HIGH = 'High'
}
export interface IVUser{
    user: string,
    stars: number
}

export interface IKata{
    name: string,
    description: string,
    level: KataLevel,
    tries: number,
    stars: {
        average: number,
        users: IVUser[]
    },
    creator: string, // ID of User creator
    solution: string,
    participants: string[]
}
