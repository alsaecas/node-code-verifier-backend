export enum RolType {
    USER = 'User',
    ADMINISTRATOR = 'Administrator'
}
export interface IUser{
    id: string,
    name: string,
    email: string,
    password: string,
    age: number,
    katas: string[],
    rol: RolType
}
