import { userEntity } from '../entities/User.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'
import { IAuth } from '../interfaces/IAuth.interface'

import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

// Enviroment variables
import dotenv from 'dotenv'
dotenv.config()
const secret = process.env.SECRETKEY || 'MYSECCRETEKEY'

// Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
  try {
    const userModel = userEntity()

    let userFound: IUser | undefined

    await userModel.findOne({ email: auth.email }).then((user: IUser) => {
      userFound = user
    }).catch((error) => {
      console.error('[ERROR in ORM]: User not Found')
      throw new Error(`[ERROR in ORM]: User not Found ${error}`)
    })
    // Check if password is valid
    const validPassword = bcrypt.compareSync(auth.password, userFound!.password)
    if (!validPassword) {
      console.error('[ERROR in ORM]: Password Not Valid')
      throw new Error('[ERROR in ORM]: Password Not Valid')
    }
    // Generate Token
    const token = jwt.sign({ id: userFound!.id, rol: userFound!.rol }, secret, {
      expiresIn: '3h'
    })
    return {
      user: userFound,
      token: token
    }
  } catch (error) {
    LogError(`[ERROR in ORM]: User not Found ${error}`)
  }
}

// Logout user
export const logoutUser = async (auth: IAuth): Promise<any | undefined> => {
}
