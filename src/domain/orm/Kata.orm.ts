import { kataEntity } from '../entities/Kata.entity'
import { LogError } from '../../utils/logger'
import { IKata } from '../interfaces/IKata.interface'
import dotenv from 'dotenv'
import { userEntity } from '../entities/User.entity'
import { response } from 'express'
dotenv.config()

// CRUD
/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */
export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // Search all katas
    await kataModel.find({ isDelete: false })
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })
    // Count total documents
    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Katas: ${error}`)
  }
}

// -Get Kata By ID
export const getKataByID = async (id: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    // Search Kata by ID
    return await kataModel.findById(id)
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Kata by ID: ${error}`)
  }
}

// -Delete Kata By ID
export const deleteKataByID = async (id: string, userID: any, userRol: any): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    const kataModel = kataEntity()
    // Delete Kata by ID
    await kataModel.findById(id).then(async (kataToDelete:any) => {
      if (userID == kataToDelete.creator || userRol === 'Administrator') {
        await kataModel.findByIdAndDelete(id)
        return await userModel.findByIdAndUpdate(userID, { $pull: { katas: { $elemMatch: id } } })
      } else {
        return await ({
          message: 'You are not allow to delete this Kata'
        })
      }
    })
  } catch (error) {
    LogError(`[ORM ERROR]: Delete Kata by ID: ${error}`)
  }
}

// -Create New Kata
export const createKata = async (kata: IKata): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    const kataModel = kataEntity()
    // Create Kata
    await kataModel.create(kata).then(async (kataCreated:any) => {
      return await userModel.findByIdAndUpdate(kata.creator, { $push: { katas: kataCreated.id } })
    })
  } catch (error) {
    LogError(`[ORM ERROR]: Creating Kata: ${error}`)
  }
}

// -Update Kata By ID
export const updateKataByID = async (id: string, kata: IKata, userID: any, userRol: any): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    // Update Kata
    await kataModel.findById(id).then(async (kataToUpdate:any) => {
      if (userID == kataToUpdate.creator || userRol === 'Administrator') {
        await kataModel.findByIdAndUpdate(id, { name: kata.name, description: kata.description, leve: kata.level, solution: kata.solution })
      } else {
        return await ({
          message: 'You are not allow to update this Kata'
        })
      }
    })
  } catch (error) {
    LogError(`[ORM ERROR]: Updating Kata ${id} error: ${error}`)
  }
}

// -Get Kata By Level
export const getKataByLevel = async (level: number, page: number, limit: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // Search Kata by Level
    await kataModel.findOne({ level: level })
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })
    // Count total documents
    await kataModel.findOne({ level: level }).countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Kata by Level: ${error}`)
  }
}

// -Get Kata By Stars
export const getKataByStars = async (stars: number, page: number, limit: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // Search Kata by Level
    await kataModel.findOne({ stars: stars })
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })
    // Count total documents
    await kataModel.findOne({ stars: stars }).countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Kata by Stars: ${error}`)
  }
}

// -Get all Katas sortened by Level
export const getKataSortLevel = async (direction: number, page: number, limit: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // Search Kata by Level
    await kataModel.find({ isDelete: false }).sort({ level: direction }).limit(5)
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })
    // Count total documents
    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Katas sortened by Level: ${error}`)
  }
}

// -Get all Katas sortened by Stars
export const getKataSortStars = async (direction: number, page: number, limit: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // Search Kata by Level
    await kataModel.find({ isDelete: false }).sort({ stars: direction }).limit(5)
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })
    // Count total documents
    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Katas sortened by Stars: ${error}`)
  }
}

// -Vote Kata By ID
export const voteKataByID = async (id: string, vote: any, userID: any): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}
    // First find kata by Kata
    await kataModel.findById(id).then(async (kataToVote:any) => {
      if (kataToVote.participants.indexOf(userID) !== -1) {
        const oldStars = kataToVote.stars.average
        const totalVotes: any[] = kataToVote.stars.users
        const newStars = ((+oldStars * +totalVotes.length) + +vote) / (+totalVotes.length + 1)
        response.kata = await kataModel.findByIdAndUpdate(id, { 'stars.average': newStars, $push: { 'stars.users': { user: userID, stars: vote } } })
      } else {
        response.message = 'You cannot update this Kata'
      }
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Voting Kata ${id} error: ${error}`)
  }
}

// -Solve Kata By ID
export const solveKataByID = async (id: string, vote: any, userID: any): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    // First find kata by Kata
    await kataModel.findByIdAndUpdate(id, { $push: { participants: userID } }).then(async (kataToSolve:any) => {
      return {
        message: kataToSolve.solution
      }
    })
  } catch (error) {
    LogError(`[ORM ERROR]: Voting Kata ${id} error: ${error}`)
  }
}

// NOT USED
// NOT USED
// NOT USED
// NOT USED

// -Get lasts Katas
export const getLastsKatas = async (): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    // Search lasts katas
    return await kataModel.find({ isDelete: false }).sort({ _id: -1 }).limit(5)
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Latsts Katas: ${error}`)
  }
}

// -Get bests Katas
export const getBestsKatas = async (): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    // Search bests katas
    return await kataModel.find({ isDelete: false }).sort({ 'valoration.value': -1 })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Bests Katas: ${error}`)
  }
}

// -Get Katas sortened by tries
export const getTriesKatas = async (): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    // Search Katas sortened by tries
    return await kataModel.find({ isDelete: false }).sort({ chances: -1 })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting Katas sortened by chances: ${error}`)
  }
}
