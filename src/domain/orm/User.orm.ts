import { userEntity } from '../entities/User.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'

import { kataEntity } from '../entities/Kata.entity'
import { IKata } from '../interfaces/IKata.interface'

// CRUD
/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (page: number, limit: number): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    const response: any = {}
    // Find all users (using page an limit)
    await userModel.find({ isDelete: false })
      .select('name email age katas')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((users: IUser[]) => {
        response.users = users
      })
      // Count total documents
    await userModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
    // Search all users
    // return await userModel.find({ isDelete: false })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users: ${error}`)
  }
}

// -Get User By ID
export const getUserByID = async (id: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Search User by ID
    return await userModel.findById(id).select('name email age katas')
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by ID: ${error}`)
  }
}

// -Get User By Name OR Email
export const getUserByNameOrEmail = async (name: string, email: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Search User by Name
    return await userModel.findOne({ $or: [{ name: { $regex: new RegExp(name, 'i') } }, { email: { $regex: new RegExp(email, 'i') } }] })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by Name: ${error}`)
  }
}

// -Delete User By ID
export const deleteUserByID = async (id: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Delete User by ID
    return await userModel.deleteOne({ _id: id })
  } catch (error) {
    LogError(`[ORM ERROR]: Delete User by ID: ${error}`)
  }
}

// -Update User By ID
export const updateUserByID = async (id: string, user: any): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Update User
    return await userModel.findByIdAndUpdate(id, user)
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${id} error: ${error}`)
  }
}

// Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Create User
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`)
  }
}

// -Get all katas from a User
export const getKatasFromUser = async (page: number, limit: number, id: string): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    const kataModel = kataEntity()
    const response: any = {}
    // Find all users (using page an limit)
    await userModel.findById(id).then(async (user: IUser) => {
      response.user = user.email
      await kataModel.find({ _id: { $in: user.katas } }).then((katas: IKata[]) => {
        response.katas = katas
      })
    }).catch((error) => {
      LogError(`[ORM ERROR]: Obtaining User ${error}`)
    })
    // // Count total documents
    // await userModel.countDocuments().then((total: number) => {
    //   response.totalPages = Math.ceil(total / limit)
    //   response.currentPage = page
    // })
    return response
    // Search all users
    // return await userModel.find({ isDelete: false })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users: ${error}`)
  }
}
