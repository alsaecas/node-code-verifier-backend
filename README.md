# Curso de Intensivo Abril - MERN

API Restful Express + Nodemon + Jest + TS + Swagger + Mongoose

## Dependencias instaladas
* _"dotenv": "^16.0.0"_: sirve para habilitar el uso de las variables de entorno desde el archivo .env.

* _"express": "^4.17.3"_: web framework para utilizar Node en nuestro programa.
* _"@types/express": "^4.17.13"_: dependencia de express para typescript

* _"nodemon": "^2.0.15"_: reinicia la aplicacion de node automaticamente si ha habido algun cambio.

* _"typescript": "^4.6.3"_: habilita typescript en el proyecto.

* _"concurrently": "^7.1.0"_: para ejecutar varios comandos al mismo tiempo desde el terminal o scripts.

* _"eslint": "^7.32.0"_: para añadir una serie de reglas de codigo limpio y buenas practicas al proyecto.
* _"eslint-config-standard": "^16.0.3"_: para configurar eslint.
* _"eslint-plugin-import": "^2.26.0"_: para prevenir problems con la sintaxis de imports.
* _"eslint-plugin-node": "^11.1.0"_: reglas especiales de node para eslint.
* _"eslint-plugin-promise": "^5.2.0"_: reglas de buenas practicas para javascript.
* _"@typescript-eslint/eslint-plugin": "^5.18.0"_: dependencia de eslint para typescript.
* _"@typescript-eslint/parser": "^5.18.0"_: dependencia extra de eslint para typescript.

* _"jest": "^27.5.1"_: para realizar los test de codigo.
* _"ts-jest": "^27.1.4"_: version typescript de jest.
* _"ts-node": "^10.7.0"_: version typescript de node.
* _"@types/jest": "^27.4.1"_: dependencia de jest para typescript.

* _"supertest": "^6.2.2"_: para proporcionar un alto grado de abstraccion de los test.

* _"webpack": "^5.71.0"_: empaque la solucion y la hace mas ligera.
* _"webpack-cli": "^4.9.2"_: para ejecutar webpack.
* _"webpack-node-externals": "^3.0.0"_: para definir modulos externos que no deberian de ser incluidos por webpack
* _"webpack-shell-plugin": "^0.5.0"_: para configurar webpack.

* _"serve": "^13.0.2"_: sirve los resultados del test en el localhost para analizarlos.

* _"@types/node": "^17.0.23"_: dependencia de node para typescript.

* _"helmet": "^5.0.2"_: añade seguridad a la solución.

* _"cors": "^2.8.5"_: añade seguridad a la solución.

* _"mongoose": "^6.2.10"_: sirve para habilitar la interacción con MongoDB.

* _"ts-loader": "^9.2.8"_: para el webpack.

* _"swagger-jsdoc": "^6.2.0"_: para habilitar la documentacion del programa con swagger.
* _"swagger-ui-express": "^4.3.0"_: para habilitar la documentacion del programa con swagger.
* _"@types/swagger-jsdoc": "^6.0.1"_: los types necesarios apra usar swagger.
* _"@types/swagger-ui-express": "^4.1.3"_: types necesarios apra usar swagger.
* _"tsoa": "^3.14.1"_: para generar un json de swagger para luego crear la documentacion en html


## Scripts de NPM
Los scripts de npm configurados en el packaje.json son:
* _build_: genera el index.js a partir del index.ts, teniendo en cuenta los parametros configurados en el tsconfig
* _start_: ejecuta el index.js de la carpeta dist, el codigo transpilado por el tsc.
* _dev_: comprueba si ha habido algun cambio y automaticamente transpila el codigo, ademas el index.js esta siendo ejecutado continuamente usando nodemon.
* _test_: ejecuta los test configurados en la carpeta tests.
* _serve:coverage_: ejecuta los tests y sirve los reportes para que pueden ser abiertos y analizados desde el explorador.


## Variables de entorno del .env
Las variables de entorno utilizadas son:
* __PORT__: es el puerto utilizado por la API en el localhost. Por defecto se utilizará el puerto 8000.
